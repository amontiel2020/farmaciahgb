<?php 
require_once('../class/Expired.php');
$expireds = $expired->all_expired();

 ?>
 <h2>Medicamentos expirados</h2>
<div class="table-responsive">
        <table id="myTable-expired" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th><center>Nome</center></th>
                    <!--<th><center>Preço</center></th>-->
                    <th><center>Quantidade</center></th>
                    <th><center>Data de validade</center></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($expireds as $ex): ?>
                <tr align="center" class="text-danger">
                    <td><?= ucwords($ex['exp_itemName']); ?></td>
                  <!--  <td><?= "$ ".number_format($ex['exp_itemPrice'], 2); ?></td> -->
                    <td><?= $ex['exp_itemQty']; ?></td>
                    <td><?= $ex['exp_expiredDate']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
</div>


<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

<!-- for the datatable of employee -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable-expired').DataTable();
    });
</script>

<?php 
$expired->Disconnect();
 ?>