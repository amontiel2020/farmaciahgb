<?php 
require_once('../class/Sales.php');

$date = $_GET['date'];
$dailySales = $sales->daily_sales($date);


 ?>
<br />
<div class="table-responsive">
        <table id="myTable-sales" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nome</th>
                    <th>Fabricante</th>
                    <th><center>Gramas</center></th>
                    <th><center>Tipo</center></th>
                   <!-- <th><center>Preço</center></th> -->
                    <th><center>Quantidade</center></th>
                  <!--  <th><center>SubTotal</center></th>-->
                  <th><center>Destino</center></th>
                  <th><center>Responsável</center></th>
                  
                </tr>
            </thead>
            <tbody>
            <?php 
                $total = 0;
                foreach($dailySales as $ds):
                $subTotal = number_format($ds['price'] * $ds['qty'], 2);   
                $total += $subTotal; 
            ?>
                <tr>
                    <td><?= $ds['item_code']; ?></td>
                    <td><?= $ds['generic_name']; ?></td>
                    <td><?= $ds['brand']; ?></td>
                    <td><?= $ds['gram']; ?></td>
                    <td><?= $ds['type']; ?></td>
                   <!-- <td align="center"><?= number_format($ds['price'],2); ?></td> -->
                    <td align="center"><?= $ds['qty']; ?></td>
                   <!-- <td align="center"><?= $subTotal; ?></td> -->
                   <td><?= $ds['destiny']; ?></td>
                   <td><?= $ds['responsible']; ?></td>
                   
                </tr>
            <?php endforeach; ?>
            </tbody>
           <!-- <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right"><strong>TOTAL:</strong></td>
                <td align="center">
                    <strong><?= number_format($total,2); ?></strong>
                </td>
                <td></td>

            </tr>-->
        </table>
</div>


<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

<!-- for the datatable of employee -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable-sales').DataTable();
    });
</script>

<?php 
$sales->Disconnect();
 ?>