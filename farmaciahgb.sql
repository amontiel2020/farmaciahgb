-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.30 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para farmacia
CREATE DATABASE IF NOT EXISTS `farmacia` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `farmacia`;

-- Volcando estructura para tabla farmacia.cart
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `cart_qty` int NOT NULL,
  `cart_stock_id` int NOT NULL,
  `user_id` int NOT NULL,
  `cart_uniqid` varchar(35) NOT NULL,
  `destiny` varchar(50) DEFAULT NULL,
  `responsible` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `item_id` (`item_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`),
  CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla farmacia.cart: ~0 rows (aproximadamente)
DELETE FROM `cart`;

-- Volcando estructura para tabla farmacia.expired
CREATE TABLE IF NOT EXISTS `expired` (
  `exp_id` int NOT NULL AUTO_INCREMENT,
  `exp_itemName` varchar(50) NOT NULL,
  `exp_itemPrice` float NOT NULL,
  `exp_itemQty` int NOT NULL,
  `exp_expiredDate` date NOT NULL,
  PRIMARY KEY (`exp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla farmacia.expired: ~0 rows (aproximadamente)
DELETE FROM `expired`;

-- Volcando estructura para tabla farmacia.item
CREATE TABLE IF NOT EXISTS `item` (
  `item_id` int NOT NULL AUTO_INCREMENT,
  `item_name` varchar(50) NOT NULL,
  `item_price` double NOT NULL,
  `item_type_id` int NOT NULL,
  `item_code` varchar(35) NOT NULL,
  `item_brand` varchar(50) NOT NULL,
  `item_grams` varchar(20) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_type_id` (`item_type_id`),
  CONSTRAINT `item_ibfk_1` FOREIGN KEY (`item_type_id`) REFERENCES `item_type` (`item_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla farmacia.item: ~3 rows (aproximadamente)
DELETE FROM `item`;
INSERT INTO `item` (`item_id`, `item_name`, `item_price`, `item_type_id`, `item_code`, `item_brand`, `item_grams`) VALUES
	(7, 'Gabapentin', 60, 1, '557-11', 'Actavis', '400'),
	(8, 'Asa', 1, 1, '1425', 'Asa', '100'),
	(9, 'Dipirona', 1, 1, '142552', 'Dipo', '500');

-- Volcando estructura para tabla farmacia.item_type
CREATE TABLE IF NOT EXISTS `item_type` (
  `item_type_id` int NOT NULL AUTO_INCREMENT,
  `item_type_desc` varchar(50) NOT NULL,
  PRIMARY KEY (`item_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla farmacia.item_type: ~2 rows (aproximadamente)
DELETE FROM `item_type`;
INSERT INTO `item_type` (`item_type_id`, `item_type_desc`) VALUES
	(1, 'Tabletas'),
	(2, 'Jarabe');

-- Volcando estructura para tabla farmacia.sales
CREATE TABLE IF NOT EXISTS `sales` (
  `sales_id` int NOT NULL AUTO_INCREMENT,
  `item_code` varchar(35) NOT NULL,
  `generic_name` varchar(35) NOT NULL,
  `brand` varchar(35) NOT NULL,
  `gram` varchar(35) NOT NULL,
  `type` varchar(35) NOT NULL,
  `qty` int NOT NULL,
  `price` float DEFAULT NULL,
  `date_sold` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `destiny` varchar(50) DEFAULT 'CURRENT_TIMESTAMP',
  `responsible` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla farmacia.sales: ~13 rows (aproximadamente)
DELETE FROM `sales`;
INSERT INTO `sales` (`sales_id`, `item_code`, `generic_name`, `brand`, `gram`, `type`, `qty`, `price`, `date_sold`, `destiny`, `responsible`) VALUES
	(1, '557-11', 'Gabapentin', 'Actavis', '400', 'Tabletas', 5, 60, '2023-08-30 12:04:22', 'Bloco Operatorio', NULL),
	(2, '557-11', 'Gabapentin', 'Actavis', '400', 'Tabletas', 3, 60, '2023-08-30 12:04:22', 'Urgência', NULL),
	(3, '142552', 'Dipirona', 'Dipo', '500', 'Tabletas', 50, NULL, '2023-08-30 17:55:20', 'Urgência', NULL),
	(4, '142552', 'Dipirona', 'Dipo', '500', 'Tabletas', 50, NULL, '2023-08-30 17:55:20', 'Urgência', NULL),
	(5, '557-11', 'Gabapentin', 'Actavis', '400', 'Tabletas', 1, NULL, '2023-08-30 17:55:20', 'Bloco Operatorio', NULL),
	(6, '142552', 'Dipirona', 'Dipo', '500', 'Tabletas', 1000, NULL, '2023-08-30 17:57:56', 'Urgência', NULL),
	(7, '142552', 'Dipirona', 'Dipo', '500', 'Tabletas', 1000, NULL, '2023-08-30 17:57:56', 'Urgência', NULL),
	(8, '557-11', 'Gabapentin', 'Actavis', '400', 'Tabletas', 1, NULL, '2023-08-30 20:46:50', 'Bloco Operatorio', NULL),
	(9, '1425', 'Asa', 'Asa', '100', 'Tabletas', 40, NULL, '2023-08-31 09:28:54', 'Bloco Operatorio', NULL),
	(10, '1425', 'Asa', 'Asa', '100', 'Tabletas', 40, NULL, '2023-08-31 09:28:54', 'Bloco Operatorio', NULL),
	(11, '1425', 'Asa', 'Asa', '100', 'Tabletas', 100, NULL, '2023-09-01 09:23:41', 'Bloco Operatorio', NULL),
	(12, '1425', 'Asa', 'Asa', '100', 'Tabletas', 5, NULL, '2023-09-20 15:38:34', 'Bloco Operatorio', NULL),
	(13, '1425', 'Asa', 'Asa', '100', 'Tabletas', 50, NULL, '2023-09-20 16:53:14', 'Bloco Operatorio', 'karel');

-- Volcando estructura para tabla farmacia.stock
CREATE TABLE IF NOT EXISTS `stock` (
  `stock_id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `stock_qty` int NOT NULL,
  `stock_expiry` date NOT NULL,
  `stock_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stock_manufactured` date NOT NULL,
  `stock_purchased` date NOT NULL,
  PRIMARY KEY (`stock_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla farmacia.stock: ~5 rows (aproximadamente)
DELETE FROM `stock`;
INSERT INTO `stock` (`stock_id`, `item_id`, `stock_qty`, `stock_expiry`, `stock_added`, `stock_manufactured`, `stock_purchased`) VALUES
	(1, 7, 0, '2017-09-15', '2023-08-30 20:46:46', '2017-05-19', '2017-05-20'),
	(2, 8, 1055, '2023-10-08', '2023-09-20 15:38:27', '2023-08-08', '2023-08-30'),
	(3, 9, 1950, '2023-10-08', '2023-08-30 17:49:04', '2023-08-08', '2023-08-30'),
	(4, 9, 4000, '2023-08-29', '2023-08-30 17:57:51', '2023-08-08', '2023-08-30'),
	(5, 8, 50, '2023-08-29', '2023-09-20 16:50:19', '2023-08-01', '2023-08-30');

-- Volcando estructura para tabla farmacia.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_account` varchar(50) NOT NULL,
  `user_pass` varchar(35) NOT NULL,
  `user_type` int NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla farmacia.user: ~2 rows (aproximadamente)
DELETE FROM `user`;
INSERT INTO `user` (`user_id`, `user_account`, `user_pass`, `user_type`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
	(2, 'admin2', '21232f297a57a5a743894a0e4a801fc3', 1);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
